package org.tekarsh.solution;

import com.opencsv.CSVWriter;

import java.io.FileWriter;
import java.io.IOException;

public class TableWriter {
    private static CSVWriter csvWriter = null;
    private TableWriter(){

    }

    /**
     *
     * @return CSVWriter
     */
    public static CSVWriter getTableWriterInstances(){
        if(csvWriter==null){
            try {
                csvWriter = new CSVWriter(new FileWriter("./CBOE.csv"));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return csvWriter;
    }
}
