package org.tekarsh.solution;

import com.opencsv.CSVWriter;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;


/**
 * Solution for Tekarsh-SE-Task
 */
public class Solution {

    static final String targetURL= "https://finance.yahoo.com/quote/%5EVIX/history?p=%5EVIX";


    static class NoDataFoundException extends Exception {
        /**
         *
         * @param message
         */
        public NoDataFoundException(String message) {
            super(message);
        }
    }

    /**
     *
     * @param args
     * Downloaded document by Jsoup.
     * Finding table elements and takes the first one.
     * Handle table header and body from the table data
     */
    public static void main(String[] args){

        try {

            Document document = Jsoup.connect(targetURL).get();
            if(document!=null){
                Elements tables = document.getElementsByTag("table");
                if(tables!=null){
                    Element table = tables.first();
                    if(table!=null){
                        Elements tableHeaders = table.select("thead");
                        try{
                            handleTableHeader(tableHeaders);
                        }catch (NullPointerException nullPointerException){
                            System.out.println("Null pointer exception occurred!");
                        }
                        Elements tableBodies= table.select("tbody");
                        try{
                            handleTableBody(tableBodies);
                        }catch (NullPointerException nullPointerException){
                            System.out.println("Null pointer exception occurred!");
                        }
                    }else{
                        throw new NullPointerException();
                    }

                }else{
                    throw new NullPointerException();
                }

            }else{
                throw new NullPointerException();
            }
            CSVWriter csvWriter = TableWriter.getTableWriterInstances();
            if(csvWriter==null){
                throw new NullPointerException();
            }
            csvWriter.close();

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    /**
     *
     * @param tableHeaders
     * Take the table row from table header and read table data from the row
     */
    public static void  handleTableHeader(Elements tableHeaders){
        if(tableHeaders!=null){
            Element tableHeader = tableHeaders.first();
            if(tableHeader!=null){
                Elements tableHeaderRows = tableHeader.select("tr");
                if(tableHeaderRows!=null){
                    Element tableHeaderRow = tableHeaderRows.first();
                    if(tableHeaderRow!=null){
                        Elements tHeads = tableHeaderRow.select("th");
                        if(tHeads!=null){
                            int tHeadsLength = tHeads.size();
                            String[] csvHeader = new String[tHeadsLength];
                            for(int index=0;index<tHeadsLength;index++){
                                csvHeader[index] = tHeads.get(index).select("span").text();
                            }
                            CSVWriter csvWriter = TableWriter.getTableWriterInstances();
                            if(csvWriter==null){
                                throw new NullPointerException();
                            }
                            csvWriter.writeNext(csvHeader);

                        }else{
                            throw new NullPointerException();
                        }

                    }else{
                        throw new NullPointerException();
                    }
                }else{
                    throw new NullPointerException();
                }
            }else{
                throw new NullPointerException();
            }

        }else{
            throw new NullPointerException();
        }
    }

    /**
     *
     * @param tableBodies
     * @throws NoDataFoundException
     * Take each table row from table body and read table data from the rows
     */

    public static void  handleTableBody(Elements tableBodies) throws NoDataFoundException {
        if(tableBodies!=null){
            Element tableBody = tableBodies.first();
            if(tableBody!=null){
                Elements tableBodyRows = tableBody.select("tr");
                if(tableBodyRows==null){
                    throw new NullPointerException();
                }
                if(tableBodyRows.size()==0){
                    throw new NoDataFoundException("No Data Found");
                }
                for(Element tableRow:tableBodyRows){
                    Elements tableData = tableRow.select("td");
                    if(tableData!=null){
                        int tDataLength = tableData.size();
                        String[] csvData = new String[tDataLength];
                        for(int index=0;index<tDataLength;index++){
                            csvData[index] = tableData.get(index).select("span").text();
                        }
                        CSVWriter csvWriter = TableWriter.getTableWriterInstances();
                        if(csvWriter==null){
                            throw new NullPointerException();
                        }
                        csvWriter.writeNext(csvData);
                    }else{
                        throw new NullPointerException();
                    }

                }
            }else{
                throw new NullPointerException();
            }
        }else{
            throw new NullPointerException();
        }
    }

}
