# TEKARSH-SE-TASK
## TASK:
- Visit the following site: CBOE Volatility Index (^VIX) Historical Data
- You will found a html table which contains the volatility index
- The table looks like:
    - Date Open High Low Close* Adj Close** Volume
    - Feb 05, 21 21.99 22.16 21.27 22.08 22.08 -
    - Feb 04, 21 23.44 23.44 21.68 21.77 21.77 -

## WHAT TO DO:
- Build a maven project which will have a Class named Solution.
- In Solution class write a java program to scrap this table and write all the data
into a CSV file and name the CSV as CBOE.csv.
- The CSV itself should contain all header and data as identical to html table.
- All dependencies should be managed by maven.
- Your program should have handled NullPointerException properly in all possible
cases.
- If the table data is empty, throw a RuntimeException with a message “No Data
Found”.

## FOR SOME HEADS-UP:

- To scrape data you can use Jsoup or Unirest (or whatever you love).
- To Write CSV you can use OpenCSV (or whatever you love).

Please Maintain the coding convention properly.
TimeLine: Please submit the project within Feb-09-2021 Tuesday (before 7 PM)